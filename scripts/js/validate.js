$(function(){
    var errorMsg = {
        name: "O nome deve ter no mínimo 3 letras",
        email: "Utilize um endereço de e-mail válido",
        email2: "Você precisa escrever o mesmo endereço de e-mail fornecido no campo anterior",
        cellphone: "Preencha um número de telefone válido",
        password: "Sua senha deve ter no mínimo 6 caracteres e deve conter apenas números e letras"
    }
    
    var isNameValid = false;
    var isEmailValid = false;
    var isEmail2Valid = false;
    var isCellphoneValid = false;
    var isPasswordValid = false;

    var fieldClass = $(".st-form--field");
    var sendButton = $(".st-form--action-btn");

    // Mostra se o campo está válido ou inválido
    
    var showError = function(field, status){
        var status = status || "invalid";
        var fieldElement = $("#register-" + field);
        var msgContainer = fieldElement.parents(".st-form--group").find(".st-form--field-msg");
    
        var errorClass = "st-form--field_invalid";
        var validClass = "st-form--field_valid";
    
        if(status === "invalid"){
            fieldElement.removeClass(validClass).addClass(errorClass);
            msgContainer.text(errorMsg[field]);
        }
    
        if(status === "valid"){
            fieldElement.removeClass(errorClass).addClass(validClass);
            msgContainer.text('');
        }
    }

    // Valida os campos
    
    var validateName = function(field){
        var val = field.val();
        
        if(val.length < 3){
            showError("name", "invalid");
            isNameValid = false;
        } else {
            showError("name", "valid");
            isNameValid = true;
        }
    }
    
    var validateEmail = function(field){
        var val = field.val();
        var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
        if(!regex.test(val)){
            showError("email", "invalid");
            isEmailValid = false;
        } else {
            showError("email", "valid");
            isEmailValid = true;
        }
    }
    
    var validateEmail2 = function(field, prevEmail){
        var val = field.val();
        var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
        if(val !== prevEmail || !regex.test(val) || !val ){
            showError("email2", "invalid");
            isEmail2Valid = false;
        } else {
            showError("email2", "valid");
            isEmail2Valid = true;
        }
    }
    
    var validateCellphone = function(field){
        var val = field.val();
        var regex = /^(?:(?:\+|00)?(55)\s?)?(?:\(?([1-9][0-9])\)?\s?)?(?:((?:9\d|[2-9])\d{3})\-?(\d{4}))$/;
    
        if(!regex.test(val)){
            showError("cellphone", "invalid");
            isCellphoneValid = false;
        } else {
            showError("cellphone", "valid");
            isCellphoneValid = true;
        }
    }
    
    var validatePassword = function(field){
        var val = field.val();
        var regex = /^[a-z0-9]+$/i;
    
        if(!regex.test(val) || val.length < 6){
            showError("password", "invalid");
            isPasswordValid = false;
        } else {
            showError("password", "valid");
            isPasswordValid = true;
        }
    }
    

    // Manda validar cada campo assim que o o foco sai
    fieldClass.blur(function(){
        var thisRef = $(this).attr("id");

        switch(thisRef){
            case "register-name":
                validateName($(this));
                break;
            case "register-email":
                validateEmail($(this));
                break;
            case "register-email2":
                validateEmail2($(this), $("#register-email").val());
                break;
            case "register-cellphone":
                validateCellphone($(this));
                break;
            case "register-password":
                validatePassword($(this));
                break;
        }
    });

    // Não mostra as mensagens de erro padrão do browser
    var allFields = document.querySelectorAll(".st-form--field");

    for(var i = 0; i < allFields.length; i++){
        allFields[i].addEventListener("invalid", function(e){
            e.preventDefault();
        });
    }

    // Evita que o usuário cole no campo de confirmar e-mail
    $("#register-email2").on("paste", function(e){
        e.preventDefault();
    });

    // Checa se está tudo ok quando o usuário clicar no botão de Criar perfil
    sendButton.click(function(){
        if(isNameValid && isEmailValid && isEmail2Valid && isCellphoneValid && isPasswordValid){
            console.log("Seu perfil foi criado com sucesso!");
        } else {
            fieldClass.focus();
            $(".st-form--field_invalid").first().focus();
        }
    });
});